\chapter{Development}
\label{ch:development}

% Describe the development of the research tool and the decissions being made 
% along the way.

Although an initial implementation of the research tool is readily available, 
several adjustments and additions need to be made in order to set up more 
sophisticated experiments. In this chapter, several stages of the development 
process will be highlighted and the resulting functionality will be 
demonstrated.

    \section{Improving the Plot View}
    \label{sec:plot-view}

    The plot view of the research tool allows for several improvements, 
    regarding performance and clarity. First of all, changes have been made 
    the logic of the implementation to make proper use of \emph{actors}. This 
    way, parts of the plot are not redrawn when changes accur, allowing for a 
    significant increase in performance when a large amount of data is 
    rendered. Furthermore, the \emph{ticks} of the plot are displayed as a 
    product of the \gls{au}, which is arguable more meaningful than meter, 
    given the subject of the plots. The option for plotting 
    the amount of \emph{degrees} between the Sun, a planet and an asteroid has 
    been implemented. Calculating this angle at time $t$ has been implemented 
    as follows:

        \begin{align}
            \alpha(\astrosun, \epsilon, \phi, t) & = \frac{\pi}{180} \cos^{-1}{\left(\frac{\delta(\astrosun, \epsilon, t) \bullet \delta(\astrosun, \phi, t)}{||\delta(\astrosun, \epsilon, t)|| \cdot ||\delta(\astrosun, \phi, t)||}\right)} \\
            \delta(\astrosun, \sigma, t) & = p_{\astrosun}(t) - p_{\sigma}(t)
        \end{align}

    in which $\astrosun$ signifies the Sun, $\epsilon$ the asteroid and $\phi$ 
    the planet. $\delta(\astrosun, \sigma, t)$ returns the distance between 
    $\astrosun$ and $\sigma$ at time $t$. Finally, the plot view has also been 
    extended to provide full support for two-dimensional plots, allowing the 
    perspective to be specified. \autoref{fig:plot-view} includes two examples 
    of the improved plot view, both in three- and two-dimensional modus.

    \begin{figure}[ht]
        \centering
        
        \begin{subfigure}[b]{0.8\textwidth}
            \includegraphics[width=\textwidth]{fig/plot-view-2d}
            \caption{Two dimensional plot view.}
            \label{fig:plot-view-2d}
        \end{subfigure}
        
        \begin{subfigure}[b]{0.8\textwidth}
            \includegraphics[width=\textwidth]{fig/plot-view-3d}
            \caption{Three dimensional plot view.}
            \label{fig:plot-view-3d}
        \end{subfigure}

        \caption{Example plots using the improved plot view, both in two and 
                 three dimensional modus. Note the ticks expressed in 
                 multiples of the \gls{au}, the angles between the 
                 asteroids, planet and Sun and the lines indicating the 
                 original position of the asteroids. In these plots, 
                 asteroids are marked with a star symbol and origin positions 
                 with a diamond symbol.}
        \label{fig:plot-view}
    \end{figure}

    \section{Re-implementing the File View}
    \label{sec:file-view}

    Although an option for exporting data as a textual file was already 
    present in research tool, it has been re-implemented to be more 
    flexible and less complex. The file view outputs a separate file for 
    each element in the model, containing all of its positions and 
    velocities in every dimension. \autoref{tab:file-view} displays an 
    example of the data produced by the file view for a single element.

    \begin{table}[ht]
        \csvreader[
            tabular = rcc,
            table head = \toprule $t$ & $p_{\jupiter}(t)$ & $v_{\jupiter}(t)$ \\ \midrule,
            table foot = \bottomrule
        ] {data/file-view/jupiter.csv} {
            px = \px,
            py = \py,
            pz = \pz,
            vx = \vx,
            vy = \vy,
            vz = \vz
        } {\thecsvrow & (\px, \py, \pz) & (\vx, \vy, \vz)}

        \caption{Example data produced by the file view, given 10 steps of a 
                 simulation of Jupiter $\jupiter$ orbiting the Sun with a 
                 step size of one week. Note that the data has been formatted 
                 to the scientific notation for readability.}
        \label{tab:file-view}
    \end{table}

    \section{Asteroid Grids}
    \label{sec:asteroid-grids}

    In the context of this project, asteroid grids are specified as a 
    collection of asteroid, spaced evenly in a box shape of a certain 
    width, height, depth and insert. Here, the inset is defined as the 
    radius of a gap from the center of the grid, allowing inner asteroids 
    to be removed. Every asteroid orbits a center and a planet, 
    e.g. the Sun and Jupiter respectively, and has an orbital velocity. 
    Orbital velocity $\dot v$ can be calculated as follows:

    \begin{align}
        \dot v(t) & = \frac{\delta(\astrosun, \epsilon, t)}{||\delta(\astrosun, \epsilon, t)||(\frac{G m_{\astrosun}(t)}{||\delta(\astrosun, \epsilon, t)||})^2} \times (0, 0, 1) \\
        \delta(\astrosun, \epsilon, t) & = p_{\astrosun}(t) - p_{\epsilon}(t)
    \end{align}

    given gravity constant $G$, center $\astrosun$ and asteroid $\epsilon$.
    \autoref{fig:grid} provides a visual example of the initial state 
    of a simulation with an asteroid grid.

    \begin{figure}[ht]
        \centering
        \includegraphics[width=0.8\textwidth]{fig/grid}
        \caption{A plot of the initial state of a simulation containing 
                 an asteroid grid, with a spacing of $5\cdot 10^{11}$ meters, 
                 a width and height of 5 asteroids and a depth of 3 asteroids. 
                 Note that asteroids that are part of the grid are marked 
                 with a star and are excluded from the legend.}
        \label{fig:grid}
    \end{figure}

    \section{Providing a CLI}
    \label{sec:cli}

    In order to improve the flexibility of the research tool in terms of 
    setting up experiments, a \gls{cli} has been provided. The following 
    options have been made available to be configured:

    \begin{description}
        \item[Center]
            The center element of the simulation, i.e. the Sun.
        \item[Planet]
            The planet orbiting \emph{only} the center.
        \item[Asteroid]
            An asteroid orbiting both the center and the planet.
        \item[Grid]
            An asteroid grid.
        \item[Dimensions]
            The dimensions to be displayed by the plot view.
        \item[Delay]
            The delay between each step of the simulation.
        \item[Step limit]
            The limit of the amount of steps to be simulated.
        \item[Step size]
            The length of each simulation step in milliseconds.
        \item[Output directory]
            The directory to which \gls{csv} files will be written.
        \item[No visualization]
            Whether to display a plot view during simulation.
        \item[No plot]
            Whether to display the final results in a plot.
    \end{description}

    Furthermore, detailed instruction on how to utilize the \gls{cli} can 
    be viewed using the help command: \texttt{python run.py --help}.

