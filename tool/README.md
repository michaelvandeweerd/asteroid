
# Installation

It's advised to use a [virtual environment](venv) when running the simulation
to insure that there will be no conflicts between the dependencies that needed 
and the ones that are already installed on your machine. The instruction below 
assume an [Ubuntu](ubuntu) machine.

```shell script
sudo apt install python3 python3-dev python3-pip python3-venv
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

# Running

An extensive description of the available command line options can be viewed 
using the `--help` argument.

```shell script
python run.py --help
```

```text
usage: run.py [-h] -c {Sun}
              [-p {Mercury,Venus,Earth,Mars,Jupiter,Saturn,Uranus,Neptune}]
              [-a {624 Hektor,617 Patroclus}] [-D DIMENSIONS] [-s STEP]
              [-d DELAY]

Simple simulator for stars, planets and asteroids.

optional arguments:
  -h, --help            show this help message and exit
  -c {Sun}, --center {Sun}
                        add a center element
  -p {Mercury,Venus,Earth,Mars,Jupiter,Saturn,Uranus,Neptune}, --planet {Mercury,Venus,Earth,Mars,Jupiter,Saturn,Uranus,Neptune}
                        add a planet element, orbiting a center
  -a {624 Hektor,617 Patroclus}, --asteroid {624 Hektor,617 Patroclus}
                        add an asteroid, orbiting a center and planet
  -D DIMENSIONS, --dimensions DIMENSIONS
                        override the dimension mapping
  -s STEP, --step STEP  override the simulation step size in seconds
  -d DELAY, --delay DELAY
                        override the simulation delay time in seconds
```

## Example: Trojan Asteroids

Constructing a simple simulation of the Trojan asteroids in the vicinity of 
Jupiter, the following command can be executed.

```shell script
python run.py -c "Sun" -p "Jupiter" -a "624 Hektor" -a "617 Patroclus"
```

[venv]:     https://docs.python.org/3.5/library/venv.html
[ubuntu]:   https://ubuntu.com/
