from argparse import Action, ArgumentParser, Namespace

from .center import Center
from .planet import Planet, Element
from model.trojan import Hektor624, Patroclus617


class Asteroid(Action):
    """
    Action to perform when an asteroid has been specified.
    """

    # The available asteroids.
    OPTIONS = {e.LABEL: e for e in [
        Hektor624,
        Patroclus617
    ]}

    def __call__(self,
                 parser: ArgumentParser,
                 namespace: Namespace,
                 value,
                 option_string=None):
        """
        Perform the asteroid action by adding the asteroid of the specified
        label to the specified space model.

        :param parser:
            The argument parser.
        :param namespace:
            The space model to which an asteroid must be added.
        :param value:
            The label of the asteroid to add.
        :param option_string:
            The current option.
        """
        if not Center.CURRENT:
            parser.error("a center must be defined prior to an asteroid")
        if not Planet.CURRENT:
            parser.error("a planet must be defined prior to an asteroid")
        try:
            asteroid = self.get_asteroid_by_label(
                value,
                others=[Center.CURRENT,
                        Planet.CURRENT],
                center=Center.CURRENT,
                planet=Planet.CURRENT)
            if not hasattr(namespace, "elements"):
                setattr(namespace, "elements", [])
            namespace.elements.append(asteroid)
        except ValueError as error:
            parser.error(str(error))

    @staticmethod
    def get_asteroid_by_label(label, **kwargs) -> Element:
        """
        Return the asteroid element of the specified label.

        :param label:
            The label by which to identify the asteroid to return.
        :param kwargs:
            The arguments to be passed to the center asteroid's constructor.
        :return:
            The asteroid element with the specified label.
        """
        if label not in Asteroid.OPTIONS:
            raise ValueError("unknown asteroid label \"%s\"" % label)
        return Asteroid.OPTIONS[label](**kwargs)
