from argparse import Action, ArgumentParser, Namespace

from model.solar import Sun


class Center(Action):
    """
    Action to perform when a center element has been specified.
    """

    # The center to which planets can still be added.
    CURRENT = None

    # The available centers.
    OPTIONS = {e.LABEL: e for e in [
        Sun
    ]}

    def __call__(self,
                 parser: ArgumentParser,
                 namespace: Namespace,
                 value,
                 option_string=None):
        """
        Perform the center action by adding the center of the specified label
        to the specified space model.

        :param parser:
            The argument parser.
        :param namespace:
            The space model to which a center must be added.
        :param value:
            The label of the center to add.
        :param option_string:
            The current option.
        """
        center = self.get_center_by_label(value, others=[])
        if not hasattr(namespace, "elements"):
            setattr(namespace, "elements", [])
        namespace.elements.append(center)
        Center.CURRENT = center

    @staticmethod
    def get_center_by_label(label, **kwargs):
        """
        Return the center element of the specified label.

        :param label:
            The label by which to identify the element to return.
        :param kwargs:
            The arguments to be passed to the center element's constructor.
        :return:
            The center element with the specified label.
        """
        if label not in Center.OPTIONS:
            raise ValueError("unknown center label \"%s\"" % label)
        return Center.OPTIONS[label](**kwargs)
