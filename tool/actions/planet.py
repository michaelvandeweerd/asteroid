from argparse import Action, ArgumentParser, Namespace

from actions.center import Center
from model.simple import SimpleJupiter
from model.solar import *


class Planet(Action):
    """
    Action to perform when a planet has been specified.
    """

    # The planet to which asteroid can still be added.
    CURRENT = None

    # The available planets.
    OPTIONS = {e.LABEL: e for e in [
        Mercury,
        Venus,
        Earth,
        Mars,
        Jupiter,
        SimpleJupiter,
        Saturn,
        Uranus,
        Neptune
    ]}

    def __call__(self,
                 parser: ArgumentParser,
                 namespace: Namespace,
                 value,
                 option_string=None):
        """
        Perform the planet action by adding the planet of the specified label
        to the specified space model.

        :param parser:
            The argument parser.
        :param namespace:
            The space model to which a planet must be added.
        :param value:
            The label of the planet to add.
        :param option_string:
            The current option.
        """
        if not Center.CURRENT:
            parser.error("a center must be defined prior to a planet")
        planet = self.get_planet_by_label(value,
                                          others=[Center.CURRENT],
                                          center=Center.CURRENT)
        if not hasattr(namespace, "elements"):
            setattr(namespace, "elements", [])
        namespace.elements.append(planet)
        Planet.CURRENT = planet

    @staticmethod
    def get_planet_by_label(label, **kwargs) -> Element:
        """
        Return the planet element of the specified label.

        :param label:
            The label by which to identify the element to return.
        :param kwargs:
            The arguments to be passed to the planet element's constructor.
        :return:
            The planet element with the specified label.
        """
        if label not in Planet.OPTIONS:
            raise ValueError("unknown planet label \"%s\"" % label)
        return Planet.OPTIONS[label](**kwargs)
