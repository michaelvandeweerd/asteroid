import random
from argparse import Action, ArgumentParser, Namespace

import numpy as np

from model import Element
from model.space import G
from model.trojan import Hektor624, Patroclus617
from .center import Center
from .planet import Planet


class AsteroidGrid(Action):
    """
    Define a square grid of asteroids, specified as <name>:<size>:<spacing>,
    with name identifying the element, size defining the amount rows and
    columns (which are equal) and spacing defining the space between each
    element.
    """

    COLORS = [
        "blue",
        "orange",
        "green",
        "red",
        "purple",
        "brown",
        "pink",
        "olive",
        "cyan"
    ]

    def __call__(self,
                 parser: ArgumentParser,
                 namespace: Namespace,
                 value,
                 option_string=None):
        assert len(value) == 6

        if not Center.CURRENT:
            parser.error("a center must be defined prior to an asteroid grid")
        if not Planet.CURRENT:
            parser.error("a planet must be defined prior to an asteroid")

        if not hasattr(namespace, "elements"):
            setattr(namespace, "elements", [])

        label = str(value[0])

        width = int(value[1])
        height = int(value[2])
        depth = int(value[3])

        spacing = float(value[4])
        inset = int(value[5])

        if not width % 2:
            parser.error("width must be uneven")
        if not height % 2:
            parser.error("height must be uneven")
        if not depth % 2:
            parser.error("depth must be uneven")

        if spacing <= 0:
            parser.error("spacing must be greater than zero")
        if inset <= 0:
            parser.error("inset must be greater than or equal to zero")

        center = Center.CURRENT
        planet = Planet.CURRENT

        color = 0

        for w in range(width):
            x = spacing * (w - (width - 1) / 2)
            for h in range(height):
                y = spacing * (h - (height - 1) / 2)
                for d in range(depth):
                    if abs(w - (width - 1) / 2) < inset \
                           and abs(h - (height - 1) / 2) < inset \
                           and abs(h - (height - 1) / 2) < inset:
                        continue

                    z = spacing * (d - (depth - 1) / 2)

                    position = np.array((x, y, z))

                    mass = None

                    try:
                        mass = self.get_mass_by_label(label)
                    except ValueError as error:
                        parser.error(str(error))

                    distance = center.position - position
                    direction = distance / np.linalg.norm(distance)

                    # Calculate the orbital velocity normal.
                    normal = np.sqrt(
                        G * center.mass / np.linalg.norm(distance)
                    )

                    # Calculate the orbital velocity vector and rotate
                    # perpendicular to the center (counter clockwise).
                    velocity = np.cross(direction * normal, (0, 0, 1))

                    namespace.elements.append(
                        Element(
                            mass,
                            position,
                            velocity,
                            show_label=False,
                            show_angle=False,
                            show_origin=True,
                            show_origin_line=True,
                            marker="*",
                            others=[
                                center, planet
                            ],
                            center=center,
                            planet=planet,
                            color="#%06x" % random.randint(0, 0xFFFFFF)
                        )
                    )

                    color = (color + 1) % len(AsteroidGrid.COLORS)

    @staticmethod
    def get_mass_by_label(label: str) -> float:
        if label == Hektor624.LABEL:
            return Hektor624.M
        if label == Patroclus617.LABEL:
            return Patroclus617.M
        raise ValueError("unknown asteroid label \"%s\"" % label)
