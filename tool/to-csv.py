import sys

from getopt import getopt
from time import time

from model import Space, Element
from model.solar import Sun, Jupiter, AU
from model.trojan import Hektor624
from view import CSVView

s = 24 * 60 * 60  # default step size
a = int(1e2)  # default step amount

if __name__ != "__main__":
    exit(1)  # must be run as main

if not len(sys.argv) >= 2:
    exit("error: no dir specified")

d = sys.argv[1]  # the dir to contain the resulting csv

opts, args = getopt(sys.argv[2:], "s:a:h")

# parse the options
for o, v in opts:
    if o == "-s":  # step size
        s = int(v)
        if s <= 0:
            exit("error: step size cannot be smaller or equal to zero")
        continue
    if o == "-a":  # step amount
        a = int(v)
        if a <= 0:
            exit("error: step amount cannot be smaller or equal to zero")
        continue
    if o == "-h":  # help
        print("usage: python to-csv.py <dir> [-s <step-size>] [-a "
              "<step-amount>]")
        exit(0)

# MODEL SETUP #################################################################
###############################################################################

sun = Sun(others=None)
jupiter = Element(Jupiter.M,
                  (5.20336301 * AU, 0, 0),
                  (0, 1.306e4, 0),
                  Jupiter.A,
                  label=Jupiter.LABEL,
                  color=Jupiter.COLOR,
                  others=[sun])

space = Space([
    sun,
    jupiter
], s)

for i in range(-180, 180, 20):
    if i == 0:
        continue
    space.element_at_angle(i, sun, jupiter, Hektor624.M,
                           label="Trojan %d" % i,
                           others=[sun, jupiter])

###############################################################################
###############################################################################

view = CSVView(space, d)
status, report = view.report_status()

if not status:
    print(report)
    i = None
    while i not in ["y", "n"]:
        i = input("continue? (y/n) ")
    if i is "n":
        exit()

print("simulating %d steps of %d seconds " % (a, s))
print("".join("." * 100))

t = time()
p = 0

for i in range(1, a + 1):
    m = (100 * i / a) % 1
    if m <= p:  # another percentage of the steps has been reached
        p = 0
        sys.stdout.write("^")
        sys.stdout.flush()
    p = m
    space.step()

print("\r\n" "done in %d second(s)" % (time() - t))

view.draw()
