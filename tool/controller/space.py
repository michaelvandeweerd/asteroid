from model import Space
from view.listener import PlotViewListener


class SpaceController(PlotViewListener):
    """"
    Provides several operations that may be performed upon a space model.
    """

    def __init__(self, model: Space):
        self.model = model

    def on_close(self):
        self.model.stop()

    def on_pause(self):
        if self.model.idle:
            self.model.resume()
        else:
            self.model.pause()

    def on_resume(self):
        if self.model.idle:
            self.model.pause()
        else:
            self.model.resume()

    def on_step(self):
        if not self.model.idle:
            return
        self.model.step()
