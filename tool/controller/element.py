from view.listener import ElementViewListener


class ElementController(ElementViewListener):
    """"
    Provides several operations that may be performed upon an element model.
    """

    def __init__(self, model):
        self.model = model
