import numpy as np

from model.listener import ElementListener


class Element:
    """
    A celestial body having a position in space, which changes due to its
    current velocity and the mass of other bodies interacting with its own.
    """

    COUNT = 0  # the amount of elements that have been initialised

    LABEL_FORMAT = "Element %d"

    INTERACTION_LIMIT = 10  # AU

    def __init__(self,
                 mass: float,
                 position,
                 velocity,
                 acceleration=np.zeros(3), **kwargs):
        self.i = Element.COUNT
        Element.COUNT += 1

        self._mass = mass  # kg

        self._velocity_history = []
        self._position_history = []

        self.position = position  # (x, y, z) in m
        self.velocity = velocity  # (x, y, z) in m/s
        self.acceleration = acceleration  # (x, y, z) in m/s/s

        self.active = True

        self._properties = kwargs
        self._listeners = []

    def __getitem__(self, key: str):
        """
        Return the property value of the specified key.

        :param str key:
            The key of the property value to return.
        :return:
            The value of the property of the specified key.
        """
        return self._properties[key] if key in self._properties else None

    def __contains__(self, key: str):
        """
        Indicate whether the current element has a property of the specified
        key.

        :param str key:
            The key of the property that the current element does or does not
            contain.
        :return:
            A boolean indicating whether the current element contains the
            property of the specified key.
        """
        return key in self._properties

    def __eq__(self, other):
        """
        Indicate whether the current element is equal to the specified
        object.

        :param other:
            The object to compare the current element to.
        :return:
            Whether to current element is equal to the specified object.
        """
        if not isinstance(other, Element):
            return False
        return self.mass == other.mass and self._properties == other._properties

    @property
    def mass(self) -> float:
        return self._mass

    @property
    def position(self) -> np.array:
        """
        Return the position of the current element.

        :return np.array:
            The position of the current element.
        """
        assert hasattr(self, "_position"), "Position not set"
        return self._position.copy()

    @position.setter
    def position(self, value):
        """
        Set the position of the current element.

        :param value:
            The new position of the current element.
        """
        if not len(value) == 3:
            raise ValueError("Position must have exactly three dimensions")
        if hasattr(self, "_position"):
            self._position_history.append(self._position)
        self._position = np.array(value)

    @property
    def position_history(self):
        return self._position_history.copy()

    @property
    def velocity(self) -> np.array:
        """
        Return the velocity of the current element.

        :return np.array:
            The velocity of the current element.
        """
        assert hasattr(self, "_velocity"), "Velocity not set"
        return self._velocity.copy()

    @velocity.setter
    def velocity(self, value):
        """
        Set the velocity of the current element.

        :param value:
            The new velocity of the current element.
        """
        if not len(value) == 3:
            raise ValueError("Velocity must have exactly three dimensions")
        if hasattr(self, "_velocity"):
            self._velocity_history.append(self._velocity)
        self._velocity = np.array(value)

    @property
    def velocity_history(self):
        return self._velocity_history.copy()

    @property
    def acceleration(self) -> np.array:
        """
        Return the acceleration of the current element.

        :return np.array:
             The acceleration of the current element.
        """
        assert hasattr(self, "_acceleration"), "Acceleration not set"
        return self._acceleration.copy()

    @acceleration.setter
    def acceleration(self, value):
        """
        Set the acceleration of the current element.

        :param value:
            The new acceleration of the current element.
        """
        if not len(value) == 3:
            raise ValueError("Acceleration must have exactly three dimensions")
        self._acceleration = np.array(value)

    @property
    def label(self):
        """
        Return the label of the current element, which is either specified as
        a property upon initialisation, or "Element x", where x is the number
        of the current element.

        :return:
        """
        return self["label"] if "label" in self else \
            Element.LABEL_FORMAT % self.i

    def copy(self):
        """
        Return an element identical to the current element.

        :return Element:
            A copy of the current element.
        """
        properties = self._properties
        properties["label"] = self.label

        copy = Element(
            self.mass,
            self.position,
            self.velocity,
            self.acceleration, **properties
        )

        copy._position_history = self._position_history
        copy._velocity_history = self._velocity_history
        return copy

    def angle(self, element1, element2):
        """"
        Calculate the angle between the two specified elements, according to
        the current element.

        :param Element element1:
            The first element to be considered.
        :param Element element2:
            The second element to be considered.
        :return float:
            The angle between the first and second element, relative to the
            current element.
        """
        assert isinstance(element1, Element)
        assert isinstance(element2, Element)

        difference_a = element1.position - self.position
        difference_b = element2.position - self.position

        return np.degrees(
            np.arccos(
                np.dot(difference_a, difference_b) / (
                        np.linalg.norm(difference_a) *
                        np.linalg.norm(difference_b)
                ))
        )

    def interact(self, others):
        """"
        Update the acceleration of the current element based on the mass of
        the other specified elements.

        :param list others:
            The elements with which the current element should interact.
        """
        from model.space import G, AU

        if not self.active:
            return

        # Check whether the current element has achieved terminal velocity.
        if self["center"]:
            center = self["center"]
            assert isinstance(center, Element)
            escape_velocity = np.sqrt(2 * G * center.mass / (
                    np.linalg.norm(center.position - self.position)
            ))
            if np.linalg.norm(self.velocity) > escape_velocity:
                self.active = False

        # Do a quick check to assert that the current element is still
        # supposed to be active, i.e. any other element is within limits.
        if not self.active or not any(
                np.linalg.norm(
                    o.position - self.position
                ) < Element.INTERACTION_LIMIT * AU for o in others):
            self.active = False
            self.acceleration = np.zeros(3)
            self.velocity = np.zeros(3)
            return

        # Keep track of the force for every dimension (x, y, z).
        force = np.zeros(3)

        for other in others:
            assert isinstance(other, Element)

            if self is other:
                continue  # silently ignore the current element

            difference = other.position - self.position
            distance = np.linalg.norm(difference)

            # Calculate the force in newton for every dimension
            # (x, y, z) and add it to the total force.
            force = np.add(
                force, np.multiply(
                    G * self.mass * other.mass / distance ** 3, difference
                )
            )

        # Calculate the acceleration in m/s/s for every dimension (x, y, z).
        self.acceleration = np.divide(force, self.mass)

    def reposition(self, step: int):
        """
        Advance the state of the current element a specified amount of seconds
        in time.

        :param int step:
            The amount of seconds to advance the current element in time.
        """

        # Calculate the velocity in m/s for every dimension (x, y, z).
        self.velocity = np.add(self.velocity, np.multiply(
            self.acceleration, step
        ))

        # Calculate the position in m for every dimension (x, y, z).
        self.position = np.add(self.position, np.multiply(
            self.velocity, step
        ))

    def notify_update(self):
        copy = self.copy()
        for listener in self._listeners:
            assert isinstance(listener, ElementListener)
            listener.on_update_element(copy)

    def add_listener(self, listener: ElementListener):
        assert listener not in self._listeners
        self._listeners.append(listener)

    def remove_listener(self, listener: ElementListener):
        assert listener in self._listeners
        self._listeners.remove(listener)
