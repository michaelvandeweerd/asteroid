from .element import Element

AU = 1.49597870700e11  # astronomical unit


class Sun(Element):
    """"
    A star, centered at the centre of the solar system.
    """

    M = 1.98892e30
    V = (0, 0, 0)
    A = (0, 0, 0)
    P = (0, 0, 0)

    LABEL = "Sun"
    COLOR = "#FDB813"

    def __init__(self, **kwargs):
        super().__init__(Sun.M,
                         Sun.P,
                         Sun.V,
                         Sun.A,
                         label=Sun.LABEL,
                         color=Sun.COLOR,
                         size=200, **kwargs)


class Mercury(Element):
    """"
    The first planet from the Sun.
    """

    M = 3.302e23
    V = (3.486807331160998e04, 2.199531461875340e04, -1.403497399061060e03)
    A = (0, 0, 0)
    P = (2.650398935154746e10, -6.050710707137029e10, -7.375462680552009e09)

    LABEL = "Mercury"
    COLOR = "#5767B4"

    def __init__(self, **kwargs):
        super().__init__(Mercury.M,
                         Mercury.P,
                         Mercury.V,
                         Mercury.A,
                         label=Mercury.LABEL,
                         color=Mercury.COLOR, **kwargs)


class Venus(Element):
    """
    The second planet from the Sun.
    """

    M = 4.8685e24
    V = (-2.592776437530452e04, 2.350886955849453e04, 1.817875117202744e03)
    A = (0, 0, 0)
    P = (7.301922246659710e10, 7.969099588863906e10, -3.125494614657875e09)

    LABEL = "Venus"
    COLOR = "#8B91A1"

    def __init__(self, **kwargs):
        super().__init__(Venus.M,
                         Venus.P,
                         Venus.V,
                         Venus.A,
                         label=Venus.LABEL,
                         color=Venus.COLOR, **kwargs)


class Earth(Element):
    """
    The third planet from the Sun.
    """

    M = 5.97219e24
    V = (-2.972266989916934e04, -5.832973678738054e03, 3.859814223710067e-01)
    A = (0, 0, 0)
    P = (-2.820340969662028e10, 1.443718990023388e11, 6.082497243583202e04)

    LABEL = "Earth"
    COLOR = "#039FAB"

    def __init__(self, **kwargs):
        super().__init__(Earth.M,
                         Earth.P,
                         Earth.V,
                         Earth.A,
                         label=Earth.LABEL,
                         color=Earth.COLOR, **kwargs)


class Mars(Element):
    """
    The fourth planet from the Sun.
    """

    M = 6.4171e23
    V = (1.832842958090348e03, -2.214429573114187e04, -5.089066604294370e02)
    A = (0, 0, 0)
    P = (-2.463742161497051e11, -9.402480267576531e09, 5.858611195784139e09)

    LABEL = "Mars"
    COLOR = "#C1440E"

    def __init__(self, **kwargs):
        super().__init__(Mars.M,
                         Mars.P,
                         Mars.V,
                         Mars.A,
                         label=Mars.LABEL,
                         color=Mars.COLOR, **kwargs)


class Jupiter(Element):
    """"
    The fifth planet from the Sun.
    """

    M = 1.89813e27
    V = (-1.239112612075993e04, 5.266964994046739e03, 2.554481860119369e02)
    A = (0, 0, 0,)
    P = (2.683452128340137e11, 7.055332456273069e11, -8.936773050983906e09)

    LABEL = "Jupiter"
    COLOR = "#A59186"

    def __init__(self, **kwargs):
        super().__init__(Jupiter.M,
                         Jupiter.P,
                         Jupiter.V,
                         Jupiter.A,
                         label=Jupiter.LABEL,
                         color=Jupiter.COLOR,
                         size=50, **kwargs)


class Saturn(Element):
    """
    The sixth planet from the Sun.
    """

    M = 5.6834e26
    V = (-8.805412499476386e03, 4.952822233752648e03, 2.636422370427793e02)
    A = (0, 0, 0)
    P = (7.006625995087143e11, 1.168532919152337e12, -4.819606875815415e10)

    LABEL = "Saturn"
    COLOR = "#CEB8B8"

    def __init__(self, **kwargs):
        super().__init__(Saturn.M,
                         Saturn.P,
                         Saturn.V,
                         Saturn.A,
                         label=Saturn.LABEL,
                         color=Saturn.COLOR, **kwargs)


class Uranus(Element):
    """
    The seventh planet from the Sun.
    """

    M = 8.6813e25
    V = (4.279647620137761e03, 4.934906438859975e03, -3.717639905304648e01)
    A = (0, 0, 0)
    P = (2.299972794490465e12, -1.903388407513380e12, -3.688864907780504e10)

    LABEL = "Uranus"
    COLOR = "#C6D3E3"

    def __init__(self, **kwargs):
        super().__init__(Uranus.M,
                         Uranus.P,
                         Uranus.V,
                         Uranus.A,
                         label=Uranus.LABEL,
                         color=Uranus.COLOR, **kwargs)


class Neptune(Element):
    """
    The eight planet from the Sun.
    """

    M = 1.02413e26
    V = (4.341942366089562e03, 3.240590502074931e03, -1.659805761987847e02)
    A = (0, 0, 0)
    P = (2.654296529474576e12, -3.638841417491632e12, 1.377210914920068e10)

    LABEL = "Neptune"
    COLOR = "#274687"

    def __init__(self, **kwargs):
        super().__init__(Neptune.M,
                         Neptune.P,
                         Neptune.V,
                         Neptune.A,
                         label=Neptune.LABEL,
                         color=Neptune.COLOR, **kwargs)
