from .element import Element


class Hektor624(Element):
    """
    A trojan asteroid of Jupiter.
    """

    M = 9.95e18
    V = (-7.338685841381704e03, -1.041180151192802e04, -3.985098856763507e03)
    A = (0, 0, 0)
    P = (-6.297161527278802e11, 4.229801762798876e11, 7.164799512029156e10)

    LABEL = "624 Hektor"
    COLOR = "red"

    def __init__(self, **kwargs):
        super().__init__(Hektor624.M,
                         Hektor624.P,
                         Hektor624.V,
                         Hektor624.A,
                         label=Hektor624.LABEL,
                         color=Hektor624.COLOR,
                         show_origin=True,
                         show_origin_line=True,
                         marker="*", **kwargs)


class Patroclus617(Element):
    """
    A trojan asteroid of Jupiter.
    """

    M = 1.36e18 
    V = (-5.520245880424921e02, 1.429987276290577e04, 4.293521198180490e03)
    A = (0, 0, 0)
    P = (6.501246067619133e11, 9.347313086389628e10, -1.572868948423238e11)

    LABEL = "617 Patroclus"
    COLOR = "blue"

    def __init__(self, **kwargs):
        super().__init__(Patroclus617.M,
                         Patroclus617.P,
                         Patroclus617.V,
                         Patroclus617.A,
                         label=Patroclus617.LABEL,
                         color=Patroclus617.COLOR,
                         show_origin=True,
                         show_origin_line=True, **kwargs)
