from model import Element
from model.space import AU


class SimpleJupiter(Element):
    """"
    The fifth planet from the Sun, in a circular, flat orbit.
    """

    M = 1.89813e27
    V = (0, 13.07e3, 0)
    A = (0, 0, 0,)
    P = ((5.4588 + 4.9501) * AU / 2, 0, 0)

    LABEL = "Jupiter Simplified"
    COLOR = "#A59186"

    def __init__(self, **kwargs):
        super().__init__(SimpleJupiter.M,
                         SimpleJupiter.P,
                         SimpleJupiter.V,
                         SimpleJupiter.A,
                         label=SimpleJupiter.LABEL,
                         color=SimpleJupiter.COLOR,
                         size=50, **kwargs)
