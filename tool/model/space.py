import numpy as np
import random
import math

from time import time, sleep

from model.element import Element
from model.listener import SpaceListener, ElementListener

AU = 1.49597870700e11  # astronomical unit
G = 6.67426e-11  # gravity constant
C = 2.99792458e8  # light speed


class Space:
    """
    A space containing zero or more elements that interact with each other.
    """

    DEFAULT_DELAY = 0.005  # delay

    DEFAULT_STEP_LIMIT = -1
    DEFAULT_STEP_SIZE = 60 * 60 * 24 * 0.5  # half a day in seconds

    def __init__(self,
                 delay: float = DEFAULT_DELAY,
                 step_limit: int = DEFAULT_STEP_LIMIT,
                 step_size: float = DEFAULT_STEP_SIZE,
                 step: int = 0):
        self._delay = delay

        self._step_limit = step_limit
        self._step_size = step_size
        self._step = step

        self.elements = []
        self.running = False
        self.idle = False

        self._listeners = []

    @property
    def step_limit(self):
        return self._step_limit

    @property
    def step_size(self):
        return self._step_size

    def add_element(self, element: Element):
        assert isinstance(element, Element)
        self.elements.append(element)

    def copy(self,
             delay: float = None,
             step_limit: int = None,
             step_size: float = None,
             step: int = None):
        """"
        Return a space identical to the current space, containing clones of
        the elements contained within the current space.

        :return Space:
            A clone of the current space.
        """
        copy = Space(
            delay=delay or self._delay,
            step_limit=step_limit or self._step_limit,
            step_size=step_size or self._step_size,
            step=step or self._step
        )

        for element in self.elements:
            assert isinstance(element, Element)
            copy.add_element(element.copy())

        return copy

    def pause(self):
        self.idle = True

    def resume(self):
        self.idle = False

    def run(self):
        self.running = True
        self.notify_start()
        while self.running \
                and self._step_limit \
                and (
                    self._step_limit > self._step or self._step_limit == -1
                ):
            t = time()
            if not self.idle:
                self._step += 1
                self.step()
            sleep(max(self._delay + t - time(), 0))
        self.notify_finished()

    def stop(self):
        self.running = False

    def step(self):
        """
        Simulate the specified amount of steps in time.
        """
        for element in self.elements:
            assert isinstance(element, Element)

            # the elements to interact with might be specified and defaults
            # to every element in the current space
            others = element["others"] if "others" in element else self.elements

            if not others:
                continue  # the current element does not interact with anything

            element.interact(others)
        for element in self.elements:
            element.reposition(self._step_size)
        self.notify_step()

    def sph2cart(self, rad, theta, phi):
        x = rad * math.sin(theta) * np.cos(phi)
        y = rad * math.sin(theta) * np.sin(phi)
        z = rad * math.cos(theta)
        return x, y, z

    def cart2sph(self, x, y, z):
        hxy = np.hypot(x, y)
        rad = np.hypot(hxy, z)
        theta = np.arctan2(z, hxy)
        phi = np.arctan2(y, x)
        return rad, theta, phi

    def element_at_angle(self, degree: float, center, target,
                         m: float, **kwargs) -> object:
        """"
        Places a new element at the specified amount of degrees relative to
        the specified center and element. Simulates a clone of the current
        space until the specified element has traveled the required amount of
        degrees and creates a new element having the identical position and
        velocity.
        """
        from model import Element

        if center not in self.elements:
            raise Exception("the specified center element is not contained in "
                            "the current space")
        if target not in self.elements:
            raise Exception("the specified target element is not contained in "
                            "the current space")

        # Convert target to spherical
        old_rad, old_theta, old_phi = self.cart2sph(target.position[0], target.position[1], target.position[2])

        # phi of new element is target.phi + degree
        phi = (old_phi + 2 * math.pi / 360 * degree) % (2 * math.pi)

        # Initialize radius and theta to close to jupiter values
        # TODO: remove hardcoded radius
        rad = 5.2044 * AU + random.uniform(-0.01 * AU, 0.01 * AU)

        theta = math.pi / 2 + random.uniform(-0.1, 0.1)

        x, y, z = self.sph2cart(rad, theta, phi)

        # Calculate velocities
        rad_vel = math.sqrt(G * center.M / rad)
        theta_vel = (math.pi / 2) * random.uniform(0.97, 1.03)
        phi_vel = (phi + math.pi / 2) * random.uniform(0.97, 1.03)

        vx, vy, vz = self.sph2cart(rad_vel, theta_vel, phi_vel)

        # Note: random mass chosen
        element = Element(m, (vx, vy, vz), (0, 0, 0), (x, y, z), **kwargs)

        self + element
        return element
        
    def exact_element_at_angle(self, degree: float, center, target,
                               m: float, s: int=DEFAULT_STEP_SIZE, **kwargs) -> object:
        """
                Places a new element at the specified amount of degrees relative to
                the specified center and element. Simulates a clone of the current
                space until the specified element has traveled the required amount of
                degrees and creates a new element having the identical position and
                velocity.
                """
        from model import Element

        if center not in self.elements:
            raise Exception("the specified center element is not contained in "
                            "the current space")
        if target not in self.elements:
            raise Exception("the specified target element is not contained in "
                            "the current space")

        clone = self.copy
        clone_center = None
        clone_target = None

        # find the clone of the specified center and target
        for element in clone.elements:
            if clone_center and clone_target:
                break
            elif not clone_center and element == center:
                clone_center = element
            elif not clone_target and element == target:
                clone_target = element

        # simulate the clone until the specified angle has been reached
        # while clone_center.angle(clone_target, target)[2] < degree:
        #     clone.step(s)
        c = 0
        rad1, theta1, phi1 = self.cart2sph(target.position[0], target.position[1], target.position[2])
        cnt = 0
        while True:
            if c == 100:
                c = 0
            rad2, theta2, phi2 = self.cart2sph(clone_target.p[0], clone_target.p[1], clone_target.p[2])
            a = (phi2 - phi1) / 2 * math.pi * 360
            if round(a) == degree * 10:
                break
            clone.step(s)
            c += 1
            cnt += 1

        # use the state of the cloned target to construct a new element
        # having the specified properties
        v, a, p = clone_target.v, clone_target.a, clone_target.p
        element = Element(m, v, a, p, **kwargs)
        print(cnt)
        self + element
        return element

    def notify_start(self):
        copy = self.copy()
        for listener in self._listeners:
            assert isinstance(listener, SpaceListener)
            listener.on_start(copy)

    def notify_step(self):
        copy = self.copy()
        for listener in self._listeners:
            assert isinstance(listener, SpaceListener)
            listener.on_step(copy, self._step)

    def notify_finished(self):
        copy = self.copy()
        for listener in self._listeners:
            assert isinstance(listener, SpaceListener)
            listener.on_finished(copy)

    def add_listener(self, listener: SpaceListener):
        self._listeners.append(listener)

    def remove_listener(self, listener: SpaceListener):
        self._listeners.remove(listener)

    def add_element_listener(self, listener: ElementListener):
        for element in self.elements:
            assert isinstance(element, Element)
            element.add_listener(listener)
