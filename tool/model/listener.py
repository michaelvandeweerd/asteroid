
class SpaceListener:

    def on_start(self, space):
        raise NotImplementedError()

    def on_step(self, space, step: int):
        raise NotImplementedError()

    def on_finished(self, space):
        raise NotImplementedError()


class ElementListener:

    def on_update_element(self, element):
        raise NotImplementedError()
