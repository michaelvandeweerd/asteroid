import os
from argparse import ArgumentParser

from actions import Center, Planet, Asteroid, AsteroidGrid
from model import Space
from view import PlotView, FileView, TerminalView
from controller import SpaceController

from threading import Thread

# Parse the model that has been specified in the arguments.
parser = ArgumentParser(
    description="Simple simulator for stars, planets and asteroids.")

parser.add_argument("-c", "--center",
                    action=Center,
                    choices=Center.OPTIONS,
                    required=True,
                    help="add a center element")

parser.add_argument("-p", "--planet",
                    action=Planet,
                    choices=Planet.OPTIONS,
                    help="add a planet element, orbiting a center")

parser.add_argument("-a", "--asteroid",
                    action=Asteroid,
                    choices=Asteroid.OPTIONS,
                    help="add an asteroid, orbiting a center and planet")

parser.add_argument("-g", "--grid",
                    action=AsteroidGrid,
                    nargs=6,
                    metavar=("NAME", "WIDTH", "HEIGHT", "DEPTH", "SPACING",
                             "INSET"),
                    help="define a grid of asteroids with a name, width, "
                         "height, depth and spacing at orbital velocity")

parser.add_argument("-D", "--dimensions",
                    default="xyz",
                    help="override the dimension mapping")

parser.add_argument("-d", "--delay",
                    help="simulation delay time in seconds",
                    type=float)

parser.add_argument("-n", "--step_limit",
                    help="amount of steps to simulate",
                    type=int)

parser.add_argument("-s", "--step_size",
                    default=Space.DEFAULT_STEP_SIZE,
                    help="simulation step size in seconds",
                    type=float)

parser.add_argument("-o", "--output-dir",
                    help="output directory for CSV files",
                    type=str)

parser.add_argument("-v", "--no-visualize",
                    default=False,
                    help="disable visualization",
                    action="store_true")

parser.add_argument("-w", "--no-plot",
                    default=False,
                    help="disable plot",
                    action="store_true")

namespace = parser.parse_args()

assert hasattr(namespace, "elements")
assert hasattr(namespace, "dimensions")

assert hasattr(namespace, "delay")

assert hasattr(namespace, "step_limit")
assert hasattr(namespace, "step_size")

assert hasattr(namespace, "output_dir")
assert hasattr(namespace, "no_visualize")
assert hasattr(namespace, "no_plot")

if namespace.delay is None:
    namespace.delay = 0 if namespace.no_visualize else Space.DEFAULT_DELAY
if namespace.step_limit is None:
    namespace.step_limit = 100 if namespace.no_visualize \
        else Space.DEFAULT_STEP_LIMIT

space = Space(
    delay=namespace.delay,
    step_limit=namespace.step_limit,
    step_size=namespace.step_size
)

for element in namespace.elements:
    space.add_element(element)

if namespace.output_dir:
    if os.path.exists(namespace.output_dir):
        if not os.path.isdir(namespace.output_dir):
            parser.error("specified output directory \"%s\" is not a "
                         "directory" % namespace.output_dir)

        if any(not f.startswith(".")
               for f in os.listdir(namespace.output_dir)):
            accept = ""
            while accept not in "yn":
                accept = input("Output directory is not empty, continue? ("
                               "y/n) ").lower()
            if accept == "n":
                parser.exit()

    file_view = FileView(namespace.output_dir)
    space.add_listener(file_view)

terminal_view = TerminalView()

# Create a space controller to handle input by the user.
space_controller = SpaceController(space)

# Create a view to visualise the model.
plot_view = PlotView(namespace.dimensions)
plot_view.add_listener(space_controller)

space.add_listener(plot_view)
space.add_listener(terminal_view)

if namespace.no_visualize:
    space.run()
else:
    # Run the simulation of the space model in a thread.
    thread = Thread(target=space.run)
    thread.daemon = True
    thread.start()

if not namespace.no_plot:
    plot_view.run()
