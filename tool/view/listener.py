
class PlotViewListener:

    def on_close(self):
        raise NotImplementedError()

    def on_pause(self):
        raise NotImplementedError()

    def on_resume(self):
        raise NotImplementedError()

    def on_step(self):
        raise NotImplementedError()


class ElementViewListener:
    pass
