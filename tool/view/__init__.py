from .plot import PlotView
from .file import FileView
from .terminal import TerminalView
