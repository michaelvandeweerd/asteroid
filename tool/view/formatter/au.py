from matplotlib.ticker import ScalarFormatter


class AUFormatter(ScalarFormatter):

    def __init__(self, au: float = None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if au is None:
            from model.space import AU
            self._au = AU
        else:
            self._au = au

    def __call__(self, x, pos=None):
        return super().__call__(x / self._au)

    def set_locs(self, locs):
        super().set_locs(locs / self._au)

    def get_offset(self):
        offset = super().get_offset()
        if offset == "":
            return "AU"
        else:
            return offset + " AU"
