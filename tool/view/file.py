import os

import numpy as np
from pandas import DataFrame

from model import Space, Element
from model.listener import SpaceListener


class FileView(SpaceListener):

    def __init__(self, root: str = "data"):
        assert not os.path.exists(root) or os.path.isdir(root)
        self._root = root

    def _render(self, space: Space):
        if not os.path.exists(self._root):
            os.mkdir(self._root)
        for element in space.elements:
            assert isinstance(element, Element)
            self._render_element(element)

    def _render_element(self, element: Element):
        DataFrame(np.column_stack([
            element.position_history + element.position,
            element.velocity_history + element.velocity
        ]), columns=[
            "px", "py", "pz", "vx", "vy", "vz"
        ]).to_csv(os.path.join(
            self._root, os.path.normpath(element.label) + ".csv")
        )

    def on_start(self, space):
        pass

    def on_step(self, space, step: int):
        pass

    def on_finished(self, space):
        assert isinstance(space, Space)
        self._render(space)
