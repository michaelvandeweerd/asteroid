from queue import Queue, Empty, Full

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.collections import PathCollection
from matplotlib.lines import Line2D
from matplotlib.text import Text
from matplotlib.ticker import NullFormatter, MultipleLocator, NullLocator
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Path3DCollection, Line3D, Text3D

from model import Element, Space
from model.listener import SpaceListener
from model.space import AU
from view.formatter import AUFormatter
from view.listener import PlotViewListener


class PlotView(SpaceListener):
    """"
    Represent a visualisation of a space model, containing elements such as the
    sun and planets. Passes input from the user to a controller.
    """

    TITLE = "Plot View"

    MARKER_MAX = 200
    MARKER_MIN = 10
    MARKER_RATIO = MARKER_MAX - MARKER_MIN

    AXES_LIMIT = 10 * AU

    HISTORY = 3e3  # steps

    ANGLE_ARC_OFFSET = AU

    KEY_PAUSE = "p"
    KEY_STEP = "n"

    def __init__(self, dimensions: str = "xyz"):
        assert len(dimensions) in (2, 3)
        assert all(d in "xyz" for d in dimensions)

        def index(label: str) -> int:
            """
            Convert the specified dimension label (i.e. "x", "y" or "z") to the
            index at which it can be expected in a vector (i.e. 0, 1 or 2
            respectively).

            :param label:
                The label to convert to a dimension index.
            :return:
                The dimension index.
            """
            if label == "x":
                return 0
            if label == "y":
                return 1
            if label == "z":
                return 2

        self._dimensions = dimensions
        self._indices = [index(d) for d in dimensions]

        self._figure = plt.figure()
        self._figure.canvas.mpl_connect("close_event", self.on_close)
        self._figure.canvas.mpl_connect("key_release_event", self.on_key_press)
        self._figure.canvas.set_window_title(PlotView.TITLE)

        if len(dimensions) == 3:
            self._axes = Axes3D(self._figure)
            self._axes.set_zlabel(dimensions[2])
            self._axes.set_zlim(-PlotView.AXES_LIMIT, PlotView.AXES_LIMIT)
            self._axes.zaxis.set_major_formatter(AUFormatter(AU))
            self._axes.zaxis.set_minor_formatter(NullFormatter())
            self._axes.zaxis.set_major_locator(MultipleLocator(AU))
            self._axes.zaxis.set_minor_locator(NullLocator())
        else:
            self._axes = self._figure.add_subplot(1, 1, 1)

        self._axes.set_ylabel(dimensions[1])
        self._axes.set_ylim(-PlotView.AXES_LIMIT, PlotView.AXES_LIMIT)
        self._axes.yaxis.set_major_formatter(AUFormatter(AU))
        self._axes.yaxis.set_minor_formatter(NullFormatter())
        self._axes.yaxis.set_major_locator(MultipleLocator(AU))
        self._axes.yaxis.set_minor_locator(NullLocator())

        self._axes.set_xlabel(dimensions[0])
        self._axes.set_xlim(-PlotView.AXES_LIMIT, PlotView.AXES_LIMIT)
        self._axes.xaxis.set_major_formatter(AUFormatter(AU))
        self._axes.xaxis.set_minor_formatter(NullFormatter())
        self._axes.xaxis.set_major_locator(MultipleLocator(AU))
        self._axes.xaxis.set_minor_locator(NullLocator())

        self._artists_elements = {}
        self._artists_histories = {}
        self._artists_origins = {}
        self._artists_origins_lines = {}
        self._artists_angles_lines = {}
        self._artists_angles_arcs = {}
        self._artists_angles_labels = {}

        self._space = None

        self._listeners = []

    def run(self):
        """
        Start the animation and show the plot.
        """
        _ = FuncAnimation(self._figure, self._draw)
        plt.show()

    def _draw(self, _) -> list:
        """
        Draw the current state of space and return the resulting actors.
        """
        try:
            if not self._space:
                return []

            space = self._space

            assert isinstance(space, Space)

            # Return the actors to be updated.
            return \
                self._draw_space(space) + \
                self._draw_space_origins(space) + \
                self._draw_space_origins_line(space) + \
                self._draw_space_angles(space) + \
                self._draw_space_histories(space)
        except Empty:
            return []

    def _draw_space(self, space: Space) -> list:
        """
        Draw the current state of space in terms of element positions.

        :param space:
            The space of which to draw the elements.
        :return:
            The artists that draw the space.
        """
        # masses are used to calculate the marker size, which is determined
        # according to the ratio of each element's mass to the lowest and
        # highest element mass
        m = [element.mass for element in space.elements]

        if len(m) == 0:
            return []

        m_max, m_min = max(m), min(m)

        labels = {}
        artists = []

        for element in space.elements:
            assert isinstance(element, Element)

            if not element.active:
                continue

            position = [
                element.position[i] for i in self._indices
            ]

            if element.label not in self._artists_elements:
                options = {
                    "marker": element["marker"] or "o",
                    "s": element["size"] or 15
                }

                if "color" in element:
                    options["color"] = element["color"]

                artist = self._axes.scatter(*position, **options)
                self._artists_elements[element.label] = artist

                if "show_label" not in element or element["show_label"]:
                    labels[artist] = element.label
            else:
                artist = self._artists_elements[element.label]

                assert isinstance(artist, PathCollection)
                artist.set_offsets(position[0:2])

                if len(position) == 3:
                    assert isinstance(artist, Path3DCollection)
                    artist.set_3d_properties([position[2]], "z")

            artists.append(artist)

        if labels:
            self._axes.legend(labels.keys(), labels.values(), loc="upper left")

        return artists

    def _draw_space_origins(self, space: Space) -> list:
        """
        Draw a line from the original position of each element to the current.

        :param space:
        :return:
        """
        artists = []

        for element in space.elements:
            assert isinstance(element, Element)

            if not element.active:
                continue
            if "show_origin" not in element or not element["show_origin"]:
                continue
            if not element.position_history:
                continue

            position = [element.position_history[0][i] for i in self._indices]

            if element.label not in self._artists_origins:
                options = {
                    "marker": "d",
                    "s": 10
                }

                if "color" in element:
                    options["color"] = element["color"]

                artist = self._axes.scatter(*position, **options)
                self._artists_origins[element.label] = artist
            else:
                artist = self._artists_origins[element.label]

                assert isinstance(artist, PathCollection)
                artist.set_offsets(position[0:2])

                if len(position) == 3:
                    assert isinstance(artist, Path3DCollection)
                    artist.set_3d_properties([position[2]], "z")
        return artists

    def _draw_space_origins_line(self, space: Space) -> list:
        """
        Draw the current state of the specified space in terms of element
        position histories.

        :param space:
            The space of which to draw the histories.
        :return:
            A list of artists that draw the element histories.
        """
        artists = []

        for element in space.elements:
            assert isinstance(element, Element)

            if not element.active:
                continue
            if "show_origin_line" not in element \
                    or not element["show_origin_line"]:
                continue

            positions = [*zip(
                [element.position_history[0][i] for i in self._indices],
                [element.position[i] for i in self._indices]
            )]

            if element.label not in self._artists_origins_lines:
                options = {
                    "linestyle": "dashdot",
                    "alpha": 0.4
                }

                if "color" in element:
                    options["color"] = element["color"]

                artist = self._axes.plot(*positions, **options)[0]
                self._artists_origins_lines[element.label] = artist
            else:
                artist = self._artists_origins_lines[element.label]

                assert isinstance(artist, Line2D)
                artist.set_data(*positions[:2])

                if len(positions) == 3:
                    assert isinstance(artist, Line3D)
                    artist.set_3d_properties(positions[2])

            artists.append(artist)

        return artists

    def _draw_space_histories(self, space: Space) -> list:
        """
        Draw the current state of the specified space in terms of element
        position histories.

        :param space:
            The space of which to draw the histories.
        :return:
            A list of artists that draw the element histories.
        """
        artists = []

        for element in space.elements:
            assert isinstance(element, Element)

            if not element.active:
                continue
            if "show_history" in element and not element["show_history"]:
                continue

            history = np.rot90(element.position_history, 3)

            if len(history) == 0 or PlotView.HISTORY == 0:
                continue

            positions = [
                history[i] for i in self._indices
            ]

            if PlotView.HISTORY < history.shape[1] and not (
                    element["show_history"] or
                    element["show_history"] != "full"
            ):
                positions = [
                    h[:int(PlotView.HISTORY)] for h in positions
                ]

            if element.label not in self._artists_histories:
                options = {
                    "linestyle": "dashed",
                    "alpha": 0.2
                }

                if "color" in element:
                    options["color"] = element["color"]

                artist = self._axes.plot(*positions, **options)[0]
                self._artists_histories[element.label] = artist
            else:
                artist = self._artists_histories[element.label]

                assert isinstance(artist, Line2D)
                artist.set_data(*positions[:2])

                if len(positions) == 3:
                    assert isinstance(artist, Line3D)
                    artist.set_3d_properties(positions[2])

            artists.append(artist)

        return artists

    def _draw_space_angles(self, space: Space) -> list:
        """
        Draw the angle of the elements in the specified space.

        :param space:
            The space of which to draw the angles.
        :return:
            The artists that draw the angles of the elements.
        """
        artists = []

        for element in space.elements:
            assert isinstance(element, Element)

            if not element.active:
                continue
            if "show_angle" in element and not element["show_angle"]:
                continue
            if "center" not in element:
                continue
            if "planet" not in element:
                continue

            center = element["center"]
            planet = element["planet"]

            assert isinstance(center, Element)
            assert isinstance(planet, Element)

            artists += [
                self._draw_element_angle_line(element, center, planet),
                self._draw_element_angle_arc(element, center, planet),
                self._draw_element_angle_label(element, center, planet)
            ]

        return artists

    def _draw_element_angle_line(self,
                                 element: Element,
                                 center: Element,
                                 planet: Element) -> Line2D:
        """
        Draw the lines for the angels of the specified element in relation to
        the specified planet at the specified center.

        :param element:
            The element of which to draw the angle lines.
        :param center:
            The center at which to measure the angle.
        :param planet:
            The planet of the element.
        :return:
            The line to draw.
        """
        assert isinstance(element, Element)
        assert isinstance(center, Element)
        assert isinstance(planet, Element)

        positions = [*zip(
            [planet.position[i] for i in self._indices],
            [center.position[i] for i in self._indices],
            [element.position[i] for i in self._indices]
        )]

        if element.label in self._artists_angles_lines:
            artist = self._artists_angles_lines[element.label]

            assert isinstance(artist, Line2D)
            artist.set_data(*positions[:2])

            if len(positions) < 3:
                return artist

            assert isinstance(artist, Line3D)
            artist.set_3d_properties(positions[2])
            return artist

        options = {
            "linestyle": "dashed",
            "alpha": 0.2
        }

        if "color" in element:
            options["color"] = element["color"]

        artist = \
            self._axes.plot(*positions, **options)[0]
        self._artists_angles_lines[element.label] = artist

        return artist

    def _draw_element_angle_arc(self,
                                element: Element,
                                center: Element,
                                planet: Element) -> Line2D:
        """
        Draw the arcs for the angles of the specified element in relation to
        the specified planet at the specified center.

        :param element:
            The element of which to draw the angle arc.
        :param center:
            The center at which to measure the angle.
        :param planet:
            The planet of the element.
        :return:
            The line to draw.
        """
        assert isinstance(element, Element)
        assert isinstance(center, Element)
        assert isinstance(planet, Element)

        difference_a = planet.position - center.position
        difference_b = element.position - center.position

        differences = [*zip(
            difference_a / np.linalg.norm(
                difference_a) * PlotView.ANGLE_ARC_OFFSET,
            difference_b / np.linalg.norm(
                difference_b) * PlotView.ANGLE_ARC_OFFSET,
        )]

        positions = [differences[i] for i in self._indices]

        if element.label in self._artists_angles_arcs:
            artist = self._artists_angles_arcs[element.label]

            assert isinstance(artist, Line2D)
            artist.set_data(*positions[:2])

            if len(positions) < 3:
                return artist

            assert isinstance(artist, Line3D)
            artist.set_3d_properties(positions[2])
            return artist

        options = {
            "alpha": 0.2
        }

        if "color" in element:
            options["color"] = element["color"]

        artist = self._axes.plot(*differences, **options)[0]
        self._artists_angles_arcs[element.label] = artist

        return artist

    def _draw_element_angle_label(self,
                                  element: Element,
                                  center: Element,
                                  planet: Element) -> Text:
        """
        Draw the labels for the angles of the specified element in relation to
        the specified planet at the specified center.

        :param element:
            The element of which to draw the angle label.
        :param center:
            The center at which to measure the angle.
        :param planet:
            The planet of the element.
        :return:
            The text to draw.
        """
        assert isinstance(element, Element)
        assert isinstance(center, Element)
        assert isinstance(planet, Element)

        average = element.position + planet.position / 2
        difference = average - center.position

        offset = difference / np.linalg.norm(difference) * (
                PlotView.ANGLE_ARC_OFFSET + AU)

        position = [offset[i] for i in self._indices]

        label = u"%4.2f\N{DEGREE SIGN}" % center.angle(element, planet)

        if element.label in self._artists_angles_labels:
            artist = self._artists_angles_labels[element.label]

            assert isinstance(artist, Text)
            artist.set_text(label)
            artist.set_x(position[0])
            artist.set_y(position[1])

            if len(position) < 3:
                return artist

            assert isinstance(artist, Text3D)
            artist.set_3d_properties(position[2], None)
            return artist

        options = {
            "size": "xx-small",
            "ha": "center",
            "va": "center"
        }

        if "color" in element:
            options["color"] = element["color"]

        artist = self._axes.text(*position, label, None, **options)
        self._artists_angles_labels[element.label] = artist

        return artist

    def on_start(self, space):
        pass

    def on_step(self, space, step: int):
        self._space = space

    def on_finished(self, space):
        pass

    def on_key_press(self, event):
        if event.key == PlotView.KEY_PAUSE:
            self._notify_pause()
        if event.key == PlotView.KEY_STEP:
            self._notify_step()

    def on_close(self, _):
        """
        Handle a close event.

        :param _:
            The closing event.
        """
        self._notify_stop()

    def _notify_resume(self):
        """
        Notify all listeners that the simulation should resume.
        """
        for listener in self._listeners:
            assert isinstance(listener, PlotViewListener)
            listener.on_resume()

    def _notify_pause(self):
        """
        Notify all listeners that the simulation should be paused.
        """
        for listener in self._listeners:
            assert isinstance(listener, PlotViewListener)
            listener.on_pause()

    def _notify_stop(self):
        """
        Notify all listeners that the current view will stop running.
        """
        for listener in self._listeners:
            assert isinstance(listener, PlotViewListener)
            listener.on_close()

    def _notify_step(self):
        """
        Notify all listeners that the simulation should advance a single step.
        """
        for listener in self._listeners:
            assert isinstance(listener, PlotViewListener)
            listener.on_step()

    def add_listener(self, listener):
        """
        Register a listener.
        """
        self._listeners.append(listener)

    def remove_listener(self, listener):
        """
        Unregister a listener.
        """
        self._listeners.remove(listener)
