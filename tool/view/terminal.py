from datetime import datetime

from tqdm import tqdm

from model.listener import SpaceListener
from view import PlotView


class TerminalView(SpaceListener):

    def __init__(self):
        self._progress = None

    def on_start(self, space):
        # Display controller instructions in the terminal.
        print(
            "{p}: toggle simulation\n"
            "{n}: simulate one step\n".format(
                p=PlotView.KEY_PAUSE,
                n=PlotView.KEY_STEP
            )
        )

        if space.step_limit < 0:
            self._progress = tqdm()
        else:
            self._progress = tqdm(range(space.step_limit))

    def on_step(self, space, step: int):
        self._progress.update()

    def on_finished(self, space):
        self._progress.close()
