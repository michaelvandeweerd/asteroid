# Computing asteroid orbits

Asteroids are small bodies which are largely found in the asteroid belt between 
Mars and Jupiter. However, some are in “Earth-crossing” orbits and could 
collide with Earth with disasterous effects (incidentally “disaster” = “bad 
star”). Another group of asteroids, called the Trojans, can be found in the 
same orbit as Jupiter, but at angles of 60 with respect to the direction 
from the Sun to Jupiter. Try to simulate the behaviour of small objects within 
a simplified solar system, and find out why the Trojans are in this peculiar 
position. Also, if time allows, try to compute the uncertanties in orbits of 
Near-Earth Asteroids (NEAs).

Reference found in digital library:
 1. A. Milani (1993) The Trojan asteroid belt: Proper elements, stability, 
    chaos and families. *Celestial Mechanics and Dynamical Astronomy* 
    **57(1-2)**: 59-94.